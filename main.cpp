#include <iostream>
#include "ucm_hidden.h"

using namespace std;

// tell me if the `letter` exists in the `str`
bool contains(string str, char letter) {
    for (int i = 0; i < str.length(); i++) {
        if (str[i] == letter) {
            return true;
        }
    }
    return false;
}

void displayLetters(string letters) {
    cout << "Letters: ";

    for (int i = 0; i < letters.length(); i++) {
        cout << letters[i];
        if (i < letters.length() - 1) {
            cout << ", ";
        }
    }
    cout << endl;
}

void displayWord(string word, string letters) {
    for (int i = 0; i < word.length(); i++) {
        if (contains(letters, word[i])) {
            cout << word[i];
        } else {
            cout << "_";
        }
        
        if (i < word.length() - 1) {
            cout << " ";
        }
    }
    cout << endl << endl;
}

bool win(string word, string letters) {
    for (int i = 0; i < word.length(); i++) {
        if (!contains(letters, word[i])) {
            return false;
        }
    }
    return true;
}

// answer: C A R
// letter: C, R
int main() {
    HIDDEN_INPUT input;
    string answer = input.get();

    // conver answer to uppercase
    for (int i = 0; i < answer.length(); i++) {
        answer[i] = toupper(answer[i]);
    }

    char letter;
    bool won = false;
    string letters = "";
    cout << "Enter a letter: ";

    while (cin >> letter) {
        letter = toupper(letter);

        if (!contains(letters, letter)) {
            letters += letter;
        }
        
        displayWord(answer, letters);

        if (win(answer, letters)) {
            won = true;
            break;
        }

        displayLetters(letters);

        cout << "Enter a letter: ";
    }

    if (won) {
        cout << "Great job!" << endl;
    } else {
        cout << endl << endl;
        cout << "Better luck next time!" << endl;
        cout << "The word was: " << answer << endl;
    }

    return 0;
}